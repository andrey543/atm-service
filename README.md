# ATM-Service

Приложение хранит в таблице данные о предметной области. В нашем случае – это список ремонтов банкоматов.

## Usage

1. **Создать БД в docker-контейнере**

    ```bash
    docker run --rm -P -p 127.0.0.1:5432:5432 -e POSTGRES_PASSWORD=postgres --name pg -d postgres
    ```
    ```bash
    docker exec -it pg psql -U postgres
    ```
    ```sql
    create database atm_db;
    ```
2. **Запустить java-приложение**
    ```bash
    ./mvnw spring-boot:run
    ```
