package com.bank.atmservice.models;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.Objects;

@Entity
public class Event {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Size(min = 9, max = 9, message = "Должно быть 9 цифр")
    @NotEmpty(message = "Не должно быть пустым")
    private String caseId;
    @NotEmpty(message = "Не должно быть пустым")
    private String atmId;
    @Size(min = 10, message = "Не меньше 10 символов")
    @NotEmpty(message = "Не должно быть пустым")
    private String reasonName;
    @DateTimeFormat(pattern = "dd-MM-yyyy HH:mm")
    @NotNull
    private Date beginTime;
    @DateTimeFormat(pattern = "dd-MM-yyyy HH:mm")
    @NotNull
    private Date endTime;
    @NotEmpty(message = "Не должно быть пустым")
    private String serialNumber;
    @NotEmpty(message = "Не должно быть пустым")
    private String bankNm;
    @NotEmpty(message = "Не должно быть пустым")
    private String connectionChannel;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Event event = (Event) o;
        return Objects.equals(caseId, event.caseId) && Objects.equals(atmId, event.atmId) && Objects.equals(reasonName, event.reasonName) && Objects.equals(beginTime, event.beginTime) && Objects.equals(endTime, event.endTime) && Objects.equals(serialNumber, event.serialNumber) && Objects.equals(bankNm, event.bankNm) && Objects.equals(connectionChannel, event.connectionChannel);
    }

    @Override
    public int hashCode() {
        return Objects.hash(caseId, atmId, reasonName, beginTime, endTime, serialNumber, bankNm, connectionChannel);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCaseId() {
        return caseId;
    }

    public void setCaseId(String caseId) {
        this.caseId = caseId;
    }

    public String getAtmId() {
        return atmId;
    }

    public void setAtmId(String atmId) {
        this.atmId = atmId;
    }

    public String getReasonName() {
        return reasonName;
    }

    public void setReasonName(String reasonName) {
        this.reasonName = reasonName;
    }

    public Date getBeginTime() {
        return beginTime;
    }

    public void setBeginTime(Date beginTime) {
        this.beginTime = beginTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getBankNm() {
        return bankNm;
    }

    public void setBankNm(String bankNm) {
        this.bankNm = bankNm;
    }

    public String getConnectionChannel() {
        return connectionChannel;
    }

    public void setConnectionChannel(String connectionChannel) {
        this.connectionChannel = connectionChannel;
    }

    @Override
    public String toString() {
        return "Event{" +
                "caseId='" + caseId + '\'' +
                ", atmId='" + atmId + '\'' +
                ", reasonName='" + reasonName + '\'' +
                ", beginTime=" + beginTime +
                ", endTime=" + endTime +
                ", serialNumber='" + serialNumber + '\'' +
                ", bankNm='" + bankNm + '\'' +
                ", connectionChannel='" + connectionChannel + '\'' +
                '}';
    }
}
