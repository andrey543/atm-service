package com.bank.atmservice.controllers;

import com.bank.atmservice.models.Event;
import com.bank.atmservice.repo.EventRepository;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.ConstraintViolation;
import javax.validation.Valid;
import javax.validation.Validation;
import javax.validation.Validator;
import java.io.*;
import java.time.temporal.ChronoUnit;
import java.util.*;

@Controller
public class MainController {

    private final EventRepository eventRepository;
    private final Validator validator;

    @Autowired
    public MainController(EventRepository eventRepository) {
        this.eventRepository = eventRepository;
        this.validator = Validation.buildDefaultValidatorFactory().getValidator();
    }

    @GetMapping("/")
    public String control(Model model)
    {
        return "control";
    }

    @GetMapping("/data")
    public String data(Model model)
    {
        Iterable<Event> events = eventRepository.findAll();
        model.addAttribute("events", events);
        return "data";
    }
    @GetMapping("/data/top3-reasons")
    public String dataTop3Reasons(Model model)
    {
        Map<String, Integer> map = new HashMap<>();
        eventRepository.findAll().forEach(event -> {
            map.merge(event.getReasonName(), 1, (a, b) -> a + b);
        });
        List<Map.Entry<String, Integer>> sortedTopEvents = new ArrayList<>(map.entrySet());
        sortedTopEvents.sort(Map.Entry.comparingByValue());
        Collections.reverse(sortedTopEvents);
        if(sortedTopEvents.size() > 3)
            model.addAttribute("top3reasons", sortedTopEvents.subList(0, 3));
        else
            model.addAttribute("top3reasons", sortedTopEvents);

        return "top3reasons";
    }

    @GetMapping("/data/top3-repair-time")
    public String dataTop3RepairTime(Model model)
    {
        List<Event> events = (List<Event>) eventRepository.findAll();
        events.sort((o1, o2) -> {
            long time1 = o1.getEndTime().getTime() - o1.getBeginTime().getTime();
            long time2 = o2.getEndTime().getTime() - o2.getBeginTime().getTime();

            return Long.compare(time2, time1);
        });
        if(events.size() > 3)
            model.addAttribute("top3repairtime", events.subList(0, 3));
        else
            model.addAttribute("top3repairtime", events);

        return "top3repairtime";
    }

    @GetMapping("/data/repeat-repairs")
    public String dataRepeatRepairs(Model model)
    {
        Map<String, String> filterEvents = new HashMap<>();
        List<Event> events = (List<Event>) eventRepository.findAll();
        for(Event event : events) {
            events.forEach(e -> {
                if(e != event &&
                        e.getAtmId().equals(event.getAtmId()) &&
                        e.getReasonName().equals(event.getReasonName()) &&
                        ChronoUnit.DAYS.between(e.getEndTime().toInstant(), event.getBeginTime().toInstant()) <= 15
                )
                {
                    filterEvents.put(event.getAtmId(), event.getReasonName());
                }
            });
        }
        model.addAttribute("repeatrepairs", filterEvents);

        return "repeatrepairs";
    }

    @GetMapping("/data/{id}/edit")
    public String edit(@PathVariable(value = "id") long id, Model model)
    {
        if(!eventRepository.existsById(id))
            return "redirect:/data";

        eventRepository.findById(id).ifPresent(event -> model.addAttribute("event", event));
        return "data_edit";
    }

    @PostMapping("/data/{id}/edit")
    public String update(@Valid Event event, BindingResult bindingResult, Model model)
    {
        if(bindingResult.hasErrors())
            return "data_edit";

        eventRepository.save(event);
        return "redirect:/data";
    }

    @PostMapping("/upload")
    public String upload(@RequestParam("file") MultipartFile file, Model model) throws IOException {

        if(file.isEmpty())
        {
            return "redirect:/";
        }

        if(!file.getOriginalFilename().endsWith(".xlsx"))
        {
            model.addAttribute("errorMessage", "Неверный формат файла");
            return "control";
        }

        List<Event> existEvents = (List<Event>) eventRepository.findAll();
        List<String> errors = new ArrayList<>();

        long count = 0;
        Workbook workbook = new XSSFWorkbook(file.getInputStream());
        for(int i = 0; i < workbook.getNumberOfSheets(); ++i)
        {
            Sheet sheet = workbook.getSheetAt(i);

            List<Event> events = new ArrayList<>();
            for(int j = 1; j < sheet.getPhysicalNumberOfRows(); ++j) {
                Event event = new Event();

                Row row = sheet.getRow(j);
                event.setCaseId(handleCell(row.getCell(0)));
                event.setAtmId(handleCell(row.getCell(1)));
                event.setReasonName(handleCell(row.getCell(2)));
                event.setBeginTime(row.getCell(3).getDateCellValue());
                event.setEndTime(row.getCell(4).getDateCellValue());
                event.setSerialNumber(handleCell(row.getCell(5)));
                event.setBankNm(handleCell(row.getCell(6)));
                event.setConnectionChannel(handleCell(row.getCell(7)));

                if(isValid(event, existEvents, errors))
                    events.add(event);
            }

            if(!errors.isEmpty()) {
                model.addAttribute("errors", errors);
                return "data_errors";
            }

            count += events.size();
            eventRepository.saveAll(events);
        }

        model.addAttribute("successUpload", true);
        model.addAttribute("uploadRecords", count);
        return "control";
    }

    @PostMapping("/clear")
    public String clear(Model model)
    {
        long count = eventRepository.count();
        eventRepository.deleteAll();
        model.addAttribute("deletedRecords", count);
        model.addAttribute("successDelete", true);
        return "control";
    }

    private boolean isValid(Event event, List<Event> existEvents, List<String> errors)
    {
        if(existEvents.contains(event))
            return false;

        Set<ConstraintViolation<Event>> violations = validator.validate(event);
        if(violations.isEmpty())
            return true;

        for(ConstraintViolation<Event> violation : violations) {
            errors.add(event.toString() + "(" + violation.getInvalidValue() + " " + violation.getMessage() + ")");
        }

        return false;
    }

    private String handleCell(Cell cell)
    {
        if(cell == null)
            return "";

        if(cell.getCellType() == Cell.CELL_TYPE_NUMERIC)
            cell.setCellType(Cell.CELL_TYPE_STRING);

        return cell.getStringCellValue();
    }
}
